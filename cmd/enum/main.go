package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"text/template"

	"github.com/iancoleman/strcase"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("missed filename or const type")
		os.Exit(1)
	}
	fileName := os.Args[1]
	enumType := os.Args[2]
	if err := generate(fileName, enumType); err != nil {
		fmt.Printf("cannot generate enum: %v", err)
		os.Exit(1)
	}
}

func generate(fileName string, enumType string) error {
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, fileName, nil, parser.ParseComments)
	if err != nil {
		return err
	}

	var packageName string
	var enumValues []string

	var inType = false
	ast.Inspect(f, func(n ast.Node) bool {
		switch t := n.(type) {
		case *ast.File:
			packageName = t.Name.Name
		case *ast.GenDecl:
			if t.Tok == token.CONST {
				for _, spec := range t.Specs {
					value, ok := spec.(*ast.ValueSpec)
					if !ok {
						continue
					}
					if value.Type == nil && !inType {
						continue
					}
					if value.Type != nil {
						ident, ok := value.Type.(*ast.Ident)
						if !ok {
							continue
						}
						if ident.Name != enumType {
							inType = false
							continue
						}
					}

					inType = true
					for _, name := range value.Names {
						if name.Name == "_" {
							continue
						}
						enumValues = append(enumValues, name.Name)
					}
				}
			}
		}
		return true
	})

	if len(enumValues) > 0 {
		return generateEnum(packageName, enumType, enumValues)
	}
	return nil
}

func generateEnum(packageName string, typeName string, values []string) error {
	packageTemplate := template.Must(template.New("").Parse(`// Code generated by gitlab.com/gorib/tools/cmd/enum; DO NOT EDIT.
package {{ .PackageName }}

import "slices"

func (s {{ .TypeName}}) IsValid() bool {
	return slices.Contains([]{{ .TypeName}}{
{{- range .Values }}
	{{ printf "\t%s" . }},
{{- end }}
	}, s)
}
`))

	fileName := strcase.ToSnake(typeName)

	write, err := os.Create(fmt.Sprintf("%s_gen.go", fileName))
	if err != nil {
		return err
	}
	defer write.Close()

	return packageTemplate.Execute(write, map[string]any{
		"PackageName": packageName,
		"TypeName":    typeName,
		"Values":      values,
	})
}
